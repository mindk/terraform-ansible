FROM alpine:3.12.0

RUN set -x && \
    apk add --no-cache --virtual .build-deps \
        gcc \
        make \
        libffi-dev \
        openssl-dev \
        musl-dev \
        python3-dev && \
    apk add --no-cache \
        py3-pip \
        python3 \
        bash \
        curl \
        openssh-client \
        git \
        coreutils && \
    pip3 install pyyaml && \
    pip3 install --upgrade pip && \
    pip3 --no-cache-dir install azure-cli==2.9.1 ansible==2.9.10 && \
    apk del .build-deps && \
    curl -fsSLO https://releases.hashicorp.com/terraform/0.13.5/terraform_0.13.5_linux_amd64.zip && \
    unzip terraform_0.13.5_linux_amd64.zip -d /usr/local/bin && \
    rm terraform_*.zip && \
    chmod +x /usr/local/bin/terraform
