FROM alpine:3.12.1

RUN set -x && \
    apk add --no-cache --virtual .build-deps \
        gcc \
        make \
        libffi-dev \
        openssl-dev \
        musl-dev \
        python3-dev && \
    apk add --no-cache \
        py3-pip \
        python3 \
        bash \
        curl \
        openssh-client \
        git \
        coreutils && \
    pip3 install --upgrade pip && \
    pip3 --no-cache-dir install ansible==2.10.4 && \
    apk del .build-deps && \
    curl -fsSLO https://releases.hashicorp.com/terraform/0.14.0/terraform_0.14.0_linux_amd64.zip && \
    unzip ./terraform_0.14.0_linux_amd64.zip -d /usr/local/bin && \
    rm ./terraform_*.zip && \
    chmod +x /usr/local/bin/terraform && \
    curl -fsSLO https://get.helm.sh/helm-v3.4.1-linux-amd64.tar.gz && \
    tar -zxvf helm-v3.4.1-linux-amd64.tar.gz && \
    mv ./linux-amd64/helm /usr/local/bin/helm && \
    rm -rf ./helm-v3.4.1-linux-amd64.tar.gz && \
    curl -fsSLO https://github.com/projectcalico/calicoctl/releases/download/v3.17.0/calicoctl && \
    chmod +x ./calicoctl && \
    mv ./calicoctl /usr/local/bin/calicoctl && \
    curl -fsSLO https://storage.googleapis.com/kubernetes-release/release/v1.20.1/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    curl -fsSLO https://amazon-eks.s3.us-west-2.amazonaws.com/1.18.9/2020-11-02/bin/linux/amd64/aws-iam-authenticator && \
    chmod +x ./aws-iam-authenticator && \
    mv ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator
